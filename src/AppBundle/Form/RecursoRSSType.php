<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;


class RecursoRSSType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nombre') /**/
            ->add('descripcion') /**/
            ->add('url', 'textarea', array(
                'label' => 'URL del Recurso'))
            ->add('categoria') /**/
            ->add('tipo', ChoiceType::class, array(
                'choices' => array(
                    'Feed RSS' => 'RSS',
                    'Documento PDF' => 'PDF'),
                'choices_as_values' => true))
        ;
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\RecursoRSS'
        ));
    }
}
