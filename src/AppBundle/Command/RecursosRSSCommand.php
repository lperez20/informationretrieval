<?php

namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use AppBundle\Entity\Noticia;
use AppBundle\Form\NoticiaType;

class RecursosRSSCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('ir:recursosrss:obtener')
            ->setDescription('Obtiene todas las noticias de los recursos registrados')
            ->addOption(
                'todos',
                null,
                InputOption::VALUE_NONE,
                'If set, the task will yell in uppercase letters'
            )
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $em = $this->getContainer()->get('doctrine')->getManager();
        $recursos = $em->getRepository('AppBundle:RecursoRSS')->findAll();
        
        foreach ($recursos as $recurso) {
            $result = $em->getRepository('AppBundle:Noticia')->existeURL($recurso->getUrl());
            
            if (sizeof($result) == 0) {
                
                $noticia = new Noticia();
                $url = $recurso->getUrl();

                if ($recurso->getTipo() == 'PDF') {
                    $noticia->setFecha('00-00-0000 00:00:00'); /*Para PDFs*/

                    $noticia->setTitulo($recurso->getNombre());
                    $noticia->setUrl($recurso->getUrl());
                    $noticia->setCategoria($recurso->getCategoria());
                    $noticia->setRecurso($recurso);

                    $curl = curl_init();
                    curl_setopt($curl, CURLOPT_URL, $url);
                    curl_setopt($curl, CURLOPT_HTTPPROXYTUNNEL, 0);
                    //curl_setopt($curl, CURLOPT_PROXYUSERPWD, 'lperez20:kyomarotakamine');
                    //curl_setopt($curl, CURLOPT_PROXY, 'proxy.uc.edu.ve:5010');
                    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
                    curl_setopt($curl, CURLOPT_HEADER, false);
                    $file = curl_exec($curl);
                    curl_close($curl);

                    $em->persist($noticia);
                    $em->flush();

                    /* Guardar con un identificador unico */
                    $fp = fopen($this->getContainer()->getParameter('kernel.root_dir').'/../web/uploads/archivos/noticia '.$noticia->getId().'.pdf', 'w');
                    fwrite($fp, $file);
                    fclose($fp);

                }else{
                    $curl = curl_init();
                    curl_setopt($curl, CURLOPT_URL, $url);
                    curl_setopt($curl, CURLOPT_HTTPPROXYTUNNEL, 0);
                    //curl_setopt($curl, CURLOPT_PROXYUSERPWD, 'lperez20:kyomarotakamine');
                    //curl_setopt($curl, CURLOPT_PROXY, 'proxy.uc.edu.ve:5010');
                    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
                    curl_setopt($curl, CURLOPT_HEADER, false);
                    $xml = curl_exec($curl);
                    curl_close($curl);

                    $xml = simplexml_load_string($xml);
                    if (isset($xml->channel)) {
                        foreach ($xml->channel->item as $item) {
                            $result = $em->getRepository('AppBundle:Noticia')->existeURL($item->link);
                            if (sizeof($result) == 0) {

                                $noticia = new Noticia();
                                $noticia->setFecha($item->pubDate); /*Para PDFs*/
                                $noticia->setTitulo($item->title);
                                $noticia->setUrl($item->link);
                                $noticia->setCategoria($recurso->getCategoria());
                                $noticia->setRecurso($recurso);

                                $curl = curl_init();
                                curl_setopt($curl, CURLOPT_URL, $item->link);
                                curl_setopt($curl, CURLOPT_HTTPPROXYTUNNEL, 0);
                                //curl_setopt($curl, CURLOPT_PROXYUSERPWD, 'lperez20:kyomarotakamine');
                                //curl_setopt($curl, CURLOPT_PROXY, 'proxy.uc.edu.ve:5010');
                                curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
                                curl_setopt($curl, CURLOPT_HEADER, false);
                                $file = curl_exec($curl);
                                curl_close($curl);

                                $em->persist($noticia);
                                $em->flush();

                                $fp = fopen($this->getContainer()->getParameter('kernel.root_dir').'/../web/uploads/archivos/noticia '.$noticia->getId().'.html', 'w');
                                fwrite($fp, $file);
                                fclose($fp);
                            }
                        }
                    }
                }
            }else{
                $output->writeln('El elemento "'.$recurso->getUrl().'", ya está registrado.');
            }
        }
    }
}