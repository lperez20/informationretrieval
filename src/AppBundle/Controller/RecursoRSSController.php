<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use AppBundle\Entity\RecursoRSS;
use AppBundle\Form\RecursoRSSType;

/**
 * RecursoRSS controller.
 *
 */
class RecursoRSSController extends Controller
{
    /**
     * Lists all RecursoRSS entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $recursoRSSes = $em->getRepository('AppBundle:RecursoRSS')->findAll();

        return $this->render('recursorss/index.html.twig', array(
            'recursoRSSes' => $recursoRSSes,
        ));
    }

    /**
     * Creates a new RecursoRSS entity.
     *
     */
    public function newAction(Request $request)
    {
        $recursoRSS = new RecursoRSS();
        $form = $this->createForm('AppBundle\Form\RecursoRSSType', $recursoRSS);

        $form->remove('nombre');
        $form->remove('descripcion');
        $form->remove('categoria');

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            
            $url = $recursoRSS->getUrl();
            $curl = curl_init();
            curl_setopt($curl, CURLOPT_URL, $url);
            curl_setopt($curl, CURLOPT_HTTPPROXYTUNNEL, 0);
            /**************** PARA EL PROXY *******************/
             curl_setopt($curl, CURLOPT_PROXYUSERPWD, 'lperez20:kyomarotakamine');
             curl_setopt($curl, CURLOPT_PROXY, 'proxy.uc.edu.ve:5010');
            /**************** PARA EL PROXY *******************/
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_HEADER, false);
            $xml = curl_exec($curl);
            curl_close($curl);

            //var_dump($xml);
            //Parseando el string a xml
            $xml = simplexml_load_string($xml);

            //var_dump($xml);
            if ($xml->channel->title != null) { $recursoRSS->setNombre($xml->channel->title); }else{ $recursoRSS->setNombre('no disponible en el rss'); }
            
            if ($xml->channel->description != null) { $recursoRSS->setDescripcion($xml->channel->description); }else{ $recursoRSS->setDescripcion('no disponible en el rss'); }

            $em->persist($recursoRSS);
            $em->flush();

            return $this->redirectToRoute('recursorss_show', array('id' => $recursoRSS->getId()));
        }

        return $this->render('recursorss/new.html.twig', array(
            'recursoRSS' => $recursoRSS,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a RecursoRSS entity.
     *
     */
    public function showAction(RecursoRSS $recursoRSS)
    {
        $deleteForm = $this->createDeleteForm($recursoRSS);
        if ($recursoRSS->getTipo() == 'PDF') {
            return $this->render('recursorss/show.html.twig', array(
                'recursoRSS' => $recursoRSS,
                'delete_form' => $deleteForm->createView(),
            ));
        }else{
            //$url = $recursoRSS->getUrl();
            //$xml = simplexml_load_file($url) or die("feed not loading");

            $url = $recursoRSS->getUrl();
            $curl = curl_init();
            curl_setopt($curl, CURLOPT_URL, $url);
            curl_setopt($curl, CURLOPT_HTTPPROXYTUNNEL, 0);
            /**************** PARA EL PROXY *******************/
             curl_setopt($curl, CURLOPT_PROXYUSERPWD, 'lperez20:kyomarotakamine');
             curl_setopt($curl, CURLOPT_PROXY, 'proxy.uc.edu.ve:5010');
            /**************** PARA EL PROXY *******************/
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_HEADER, false);
            $xml = curl_exec($curl);
            curl_close($curl);

            //var_dump($xml);
            //Parseando el string a xml
            $xml = simplexml_load_string($xml);

            return $this->render('recursorss/show.html.twig', array(
                'recursoRSS' => $recursoRSS,
                'delete_form' => $deleteForm->createView(),
                'xml' => $xml
            ));
        }
    }

    /**
     * Displays a form to edit an existing RecursoRSS entity.
     *
     */
    public function editAction(Request $request, RecursoRSS $recursoRSS)
    {
        $deleteForm = $this->createDeleteForm($recursoRSS);
        $editForm = $this->createForm('AppBundle\Form\RecursoRSSType', $recursoRSS);

        $editForm->remove('tipo');
        
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($recursoRSS);
            $em->flush();

            return $this->redirectToRoute('recursorss_edit', array('id' => $recursoRSS->getId()));
        }

        return $this->render('recursorss/edit.html.twig', array(
            'recursoRSS' => $recursoRSS,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a RecursoRSS entity.
     *
     */
    public function deleteAction(Request $request, RecursoRSS $recursoRSS)
    {
        $form = $this->createDeleteForm($recursoRSS);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($recursoRSS);
            $em->flush();
        }

        return $this->redirectToRoute('recursorss_index');
    }

    /**
     * Creates a form to delete a RecursoRSS entity.
     *
     * @param RecursoRSS $recursoRSS The RecursoRSS entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(RecursoRSS $recursoRSS)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('recursorss_delete', array('id' => $recursoRSS->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
