<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use AppBundle\Entity\CategoriaRSS;
use AppBundle\Form\CategoriaRSSType;

/**
 * CategoriaRSS controller.
 *
 */
class CategoriaRSSController extends Controller
{
    /**
     * Lists all CategoriaRSS entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $categoriaRSSes = $em->getRepository('AppBundle:CategoriaRSS')->findAll();

        return $this->render('categoriarss/index.html.twig', array(
            'categoriaRSSes' => $categoriaRSSes,
        ));
    }

    /**
     * Creates a new CategoriaRSS entity.
     *
     */
    public function newAction(Request $request)
    {
        $categoriaRSS = new CategoriaRSS();
        $form = $this->createForm('AppBundle\Form\CategoriaRSSType', $categoriaRSS);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($categoriaRSS);
            $em->flush();

            return $this->redirectToRoute('categoriarss_show', array('id' => $categoriaRSS->getId()));
        }

        return $this->render('categoriarss/new.html.twig', array(
            'categoriaRSS' => $categoriaRSS,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a CategoriaRSS entity.
     *
     */
    public function showAction(CategoriaRSS $categoriaRSS)
    {
        $deleteForm = $this->createDeleteForm($categoriaRSS);

        return $this->render('categoriarss/show.html.twig', array(
            'categoriaRSS' => $categoriaRSS,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing CategoriaRSS entity.
     *
     */
    public function editAction(Request $request, CategoriaRSS $categoriaRSS)
    {
        $deleteForm = $this->createDeleteForm($categoriaRSS);
        $editForm = $this->createForm('AppBundle\Form\CategoriaRSSType', $categoriaRSS);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($categoriaRSS);
            $em->flush();

            return $this->redirectToRoute('categoriarss_edit', array('id' => $categoriaRSS->getId()));
        }

        return $this->render('categoriarss/edit.html.twig', array(
            'categoriaRSS' => $categoriaRSS,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a CategoriaRSS entity.
     *
     */
    public function deleteAction(Request $request, CategoriaRSS $categoriaRSS)
    {
        $form = $this->createDeleteForm($categoriaRSS);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($categoriaRSS);
            $em->flush();
        }

        return $this->redirectToRoute('categoriarss_index');
    }

    /**
     * Creates a form to delete a CategoriaRSS entity.
     *
     * @param CategoriaRSS $categoriaRSS The CategoriaRSS entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(CategoriaRSS $categoriaRSS)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('categoriarss_delete', array('id' => $categoriaRSS->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
