<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use AppBundle\Entity\TCF;
use AppBundle\Form\TCFType;

/**
 * TCF controller.
 *
 */
class TCFController extends Controller
{
    /**
     * Lists all TCF entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $tCFs = $em->getRepository('AppBundle:TCF')->findAll();

        return $this->render('tcf/index.html.twig', array(
            'tCFs' => $tCFs,
        ));
    }

    /**
     * Creates a new TCF entity.
     *
     */
    public function newAction(Request $request)
    {
        $tCF = new TCF();
        $form = $this->createForm('AppBundle\Form\TCFType', $tCF);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($tCF);
            $em->flush();

            return $this->redirectToRoute('tcf_show', array('id' => $tCF->getId()));
        }

        return $this->render('tcf/new.html.twig', array(
            'tCF' => $tCF,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a TCF entity.
     *
     */
    public function showAction(TCF $tCF)
    {
        $deleteForm = $this->createDeleteForm($tCF);

        return $this->render('tcf/show.html.twig', array(
            'tCF' => $tCF,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing TCF entity.
     *
     */
    public function editAction(Request $request, TCF $tCF)
    {
        $deleteForm = $this->createDeleteForm($tCF);
        $editForm = $this->createForm('AppBundle\Form\TCFType', $tCF);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($tCF);
            $em->flush();

            return $this->redirectToRoute('tcf_edit', array('id' => $tCF->getId()));
        }

        return $this->render('tcf/edit.html.twig', array(
            'tCF' => $tCF,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a TCF entity.
     *
     */
    public function deleteAction(Request $request, TCF $tCF)
    {
        $form = $this->createDeleteForm($tCF);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($tCF);
            $em->flush();
        }

        return $this->redirectToRoute('tcf_index');
    }

    /**
     * Creates a form to delete a TCF entity.
     *
     * @param TCF $tCF The TCF entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(TCF $tCF)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('tcf_delete', array('id' => $tCF->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
