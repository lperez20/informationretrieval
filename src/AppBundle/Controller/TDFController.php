<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use AppBundle\Entity\TDF;
use AppBundle\Form\TDFType;

/**
 * TDF controller.
 *
 */
class TDFController extends Controller
{
    /**
     * Lists all TDF entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $tDFs = $em->getRepository('AppBundle:TDF')->findAll();

        return $this->render('tdf/index.html.twig', array(
            'tDFs' => $tDFs,
        ));
    }

    /**
     * Creates a new TDF entity.
     *
     */
    public function newAction(Request $request)
    {
        $tDF = new TDF();
        $form = $this->createForm('AppBundle\Form\TDFType', $tDF);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($tDF);
            $em->flush();

            return $this->redirectToRoute('tdf_show', array('id' => $tDF->getId()));
        }

        return $this->render('tdf/new.html.twig', array(
            'tDF' => $tDF,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a TDF entity.
     *
     */
    public function showAction(TDF $tDF)
    {
        $deleteForm = $this->createDeleteForm($tDF);

        return $this->render('tdf/show.html.twig', array(
            'tDF' => $tDF,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing TDF entity.
     *
     */
    public function editAction(Request $request, TDF $tDF)
    {
        $deleteForm = $this->createDeleteForm($tDF);
        $editForm = $this->createForm('AppBundle\Form\TDFType', $tDF);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($tDF);
            $em->flush();

            return $this->redirectToRoute('tdf_edit', array('id' => $tDF->getId()));
        }

        return $this->render('tdf/edit.html.twig', array(
            'tDF' => $tDF,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a TDF entity.
     *
     */
    public function deleteAction(Request $request, TDF $tDF)
    {
        $form = $this->createDeleteForm($tDF);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($tDF);
            $em->flush();
        }

        return $this->redirectToRoute('tdf_index');
    }

    /**
     * Creates a form to delete a TDF entity.
     *
     * @param TDF $tDF The TDF entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(TDF $tDF)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('tdf_delete', array('id' => $tDF->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
