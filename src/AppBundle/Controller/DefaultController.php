<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        // replace this example code with whatever you need
        return $this->render('default/index.html.twig', array(
            'base_dir' => realpath($this->container->getParameter('kernel.root_dir').'/..'),
        ));
    }

    /* VISTA PRINCIPAL DEL BUSCADOR DE LA APLICACION */
    public function buscadorAction(Request $request)
    {
        $form = $request->request->all();
        
        if (array_key_exists('cadena', $form)) {
            if (strlen($form['cadena']) < 3) {
                return $this->render('default/buscador.html.twig', array(
                    'cadena' => 'Refine su búsqueda, use más palabras.'));
            }
            
            return $this->render('default/buscador.html.twig', array(
                'cadena' => $form['cadena']));
        }

        return $this->render('default/buscador.html.twig', array(
            'cadena' => 'no ha escrito ninguna consulta'));
        
    }

    /* VISTA PRINCIPAL DE LAS FUNCIONES A LAS QUE TIENE ACCESO EL ADMINISTRADOR SIMULADO*/
    public function administradorAction(Request $request)
    {
        return $this->render('default/administrador.html.twig');
    }
}
