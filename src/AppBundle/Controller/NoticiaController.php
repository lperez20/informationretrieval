<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints\File;
use Symfony\Component\HttpFoundation\JsonResponse;

use AppBundle\Entity\Noticia;
use AppBundle\Entity\CategoriaRSS;
use AppBundle\Form\NoticiaType;
use AppBundle\Form\CategoriaRSSType;
use AppBundle\Entity\RecursoRSS;
use AppBundle\Entity\TDF;
use AppBundle\Form\TDFType;
use AppBundle\Entity\TCF;
use AppBundle\Form\TCFType;
use AppBundle\Entity\Vector;
use AppBundle\Form\VectorType;

require_once __DIR__.'/stemm_es.php';

/**
 * Noticia controller.
 */
class NoticiaController extends Controller
{
    /**
     * Lists all Noticia entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $noticias = $em->getRepository('AppBundle:Noticia')->findAll();
        $cantidad_noticias = $em->getRepository('AppBundle:Noticia')->cantidad_de_documentos();

        return $this->render('noticia/index.html.twig', array(
            'noticias' => $noticias,
            'cantidad' => $cantidad_noticias,
        ));
    }

    /**
     * Creates a new Noticia entity.
     *
     */
    public function newAction(Request $request)
    {
        $noticium = new Noticia();
        $form = $this->createForm('AppBundle\Form\NoticiaType', $noticium);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($noticium);
            $em->flush();

            return $this->redirectToRoute('noticia_show', array('id' => $noticium->getId()));
        }

        return $this->render('noticia/new.html.twig', array(
            'noticium' => $noticium,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Noticia entity.
     *
     */
    public function showAction(Noticia $noticium)
    {
        $deleteForm = $this->createDeleteForm($noticium);

        return $this->render('noticia/show.html.twig', array(
            'noticium' => $noticium,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Noticia entity.
     *
     */
    public function editAction(Request $request, Noticia $noticium)
    {
        $deleteForm = $this->createDeleteForm($noticium);
        $editForm = $this->createForm('AppBundle\Form\NoticiaType', $noticium);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($noticium);
            $em->flush();

            return $this->redirectToRoute('noticia_edit', array('id' => $noticium->getId()));
        }

        return $this->render('noticia/edit.html.twig', array(
            'noticium' => $noticium,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a Noticia entity.
     *
     */
    public function deleteAction(Request $request, Noticia $noticium)
    {
        $form = $this->createDeleteForm($noticium);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            
            if ($noticium->getRecurso()->getTipo() == 'PDF') {
                $location = $this->container->getParameter('kernel.root_dir').'/../web/uploads/archivos/noticia '.$noticium->getId().'.pdf';
            }else{
                $location = $this->container->getParameter('kernel.root_dir').'/../web/uploads/archivos/noticia '.$noticium->getId().'.txt';
            }

            if (is_writable($location)) {
                //echo "Existe y se puede escribir en el.";
                unlink($location);
            }

            $em->remove($noticium);
            $em->flush();
        }

        return $this->redirectToRoute('noticia_index');
    }

    /**
     * Creates a form to delete a Noticia entity.
     *
     * @param Noticia $noticium The Noticia entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Noticia $noticium)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('noticia_delete', array('id' => $noticium->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }

    /* Funciones para tratar los string, eliminación de palabras vacias, remover palabras */
    private function cleanString($text)
    {
        // 1) convert á ô => a o
        $text = preg_replace("/[\",.-;:]/","",$text);
        $text = preg_replace("/á/","a",$text);
        $text = preg_replace("/à/","a",$text);
        $text = preg_replace("/â/","a",$text);
        $text = preg_replace("/ã/","a",$text);
        $text = preg_replace("/ª/","a",$text);
        $text = preg_replace("/ä/","a",$text);
        $text = preg_replace("/[ÁÀÂÃÄ]/","a",$text);
        $text = preg_replace("/[ÍÌÎÏ]/","i",$text);
        $text = preg_replace("/í/","i",$text);
        $text = preg_replace("/ì/","i",$text);
        $text = preg_replace("/î/","i",$text);
        $text = preg_replace("/ï/","i",$text);
        $text = preg_replace("/é/","e",$text);
        $text = preg_replace("/è/","e",$text);
        $text = preg_replace("/ê/","e",$text);
        $text = preg_replace("/ë/","e",$text);
        $text = preg_replace("/[ÉÈÊË]/","e",$text);
        $text = preg_replace("/ó/","o",$text);
        $text = preg_replace("/ò/","o",$text);
        $text = preg_replace("/ô/","o",$text);
        $text = preg_replace("/õ/","o",$text);
        $text = preg_replace("/º/","o",$text);
        $text = preg_replace("/ö/","o",$text);
        $text = preg_replace("/[ÓÒÔÕÖ]/","o",$text);
        $text = preg_replace("/ú/","u",$text);
        $text = preg_replace("/ù/","u",$text);
        $text = preg_replace("/û/","u",$text);
        $text = preg_replace("/ü/","u",$text);
        $text = preg_replace("/[ÚÙÛÜ]/","u",$text);
        $text = preg_replace("/[’‘‹›‚]/","'",$text);
        $text = preg_replace("/[“”«»„]/",'"',$text);
        $text = preg_replace("/&#39;/",'',$text);
        $text = str_replace("–","-",$text);
        $text = str_replace("-"," ",$text);
        $text = str_replace(" "," ",$text);
        $text = str_replace("ç","c",$text);
        $text = str_replace("Ç","c",$text);
        $text = str_replace("ñ","n",$text);
        $text = str_replace("Ñ","n",$text);
        $text = str_replace("&apos;","",$text);
     
        //2) Translation CP1252. &ndash; => -
        $trans = get_html_translation_table(HTML_ENTITIES); 
        $trans[chr(130)] = '&sbquo;';    // Single Low-9 Quotation Mark 
        $trans[chr(131)] = '&fnof;';    // Latin Small Letter F With Hook 
        $trans[chr(132)] = '&bdquo;';    // Double Low-9 Quotation Mark 
        $trans[chr(133)] = '&hellip;';    // Horizontal Ellipsis 
        $trans[chr(134)] = '&dagger;';    // Dagger 
        $trans[chr(135)] = '&Dagger;';    // Double Dagger 
        $trans[chr(136)] = '&circ;';    // Modifier Letter Circumflex Accent 
        $trans[chr(137)] = '&permil;';    // Per Mille Sign 
        $trans[chr(138)] = '&Scaron;';    // Latin Capital Letter S With Caron 
        $trans[chr(139)] = '&lsaquo;';    // Single Left-Pointing Angle Quotation Mark 
        $trans[chr(140)] = '&OElig;';    // Latin Capital Ligature OE 
        $trans[chr(145)] = '&lsquo;';    // Left Single Quotation Mark 
        $trans[chr(146)] = '&rsquo;';    // Right Single Quotation Mark 
        $trans[chr(147)] = '&ldquo;';    // Left Double Quotation Mark 
        $trans[chr(148)] = '&rdquo;';    // Right Double Quotation Mark 
        $trans[chr(149)] = '&bull;';    // Bullet 
        $trans[chr(150)] = '&ndash;';    // En Dash 
        $trans[chr(151)] = '&mdash;';    // Em Dash 
        $trans[chr(152)] = '&tilde;';    // Small Tilde 
        $trans[chr(153)] = '&trade;';    // Trade Mark Sign 
        $trans[chr(154)] = '&scaron;';    // Latin Small Letter S With Caron 
        $trans[chr(155)] = '&rsaquo;';    // Single Right-Pointing Angle Quotation Mark 
        $trans[chr(156)] = '&oelig;';    // Latin Small Ligature OE 
        $trans[chr(159)] = '&Yuml;';    // Latin Capital Letter Y With Diaeresis 
        $trans['euro'] = '&euro;';    // euro currency symbol 
        ksort($trans); 
         
        foreach ($trans as $k => $v) {
            $text = str_replace($v, $k, $text);
        }
     
        // 3) remove <p>, <br/> ...
        $text = strip_tags($text);

        //remove ascii
        //$text = preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $text);
         
        // 4) &amp; => & &quot; => '
        $text = html_entity_decode($text);
         
        // 5) remove Windows-1252 symbols like "TradeMark", "Euro"...
        $text = preg_replace('/[^(\x20-\x7F)]*/','', $text);
        
        $targets=array('\r\n','\n','\r','\t');
        $results=array(" "," "," ","");
        $text = str_replace($targets,$results,$text);
     
        //XML compatible
        /*
        $text = str_replace("&", "and", $text);
        $text = str_replace("<", ".", $text);
        $text = str_replace(">", ".", $text);
        $text = str_replace("\\", "-", $text);
        $text = str_replace("/", "-", $text);
        */
        return ($text);
    }

    private function removeCommonWords($input)
    {
        $commonWords = array('a','la','las','al','el','le','y','en','de','ya', 'un','lo','mas','su','los','se','por','del','que','qué','este','o','u','e','una','ha','unas','unos','uno','sobre','todo','también','tras','otro','algún','alguno','alguna','algunos','algunas','ser','es','soy','eres','somos','sois','estoy','esta','estamos','estais','estan','como','en','para','atras','porque','por qué','estado','estaba','ante','antes','siendo','ambos','pero','por','poder','puede','puedo','podemos','podeis','pueden','fui','fue','fuimos','fueron','hacer','hago','hace','hacemos','haceis','hacen','cada','fin','incluso','primero desde','conseguir','consigo','consigue','consigues','conseguimos','consiguen','ir','voy','va','vamos','vais','van','vaya','gueno','ha','tener','tengo','tiene','tenemos','teneis','tienen','el','la','lo','las','los','su','aqui','mio','tuyo','ellos','ellas','nos','nosotros','vosotros','vosotras','si','dentro','solo','solamente','saber','sabes','sabe','sabemos','sabeis','saben','ultimo','largo','bastante','haces','muchos','aquellos','aquellas','sus','entonces','tiempo','verdad','verdadero','verdadera   cierto','ciertos','cierta','ciertas','intentar','intento','intenta','intentas','intentamos','intentais','intentan','dos','bajo','arriba','encima','usar','uso','usas','usa','usamos','usais','usan','emplear','empleo','empleas','emplean','ampleamos','empleais','valor','muy','era','eras','eramos','eran','modo','bien','cual','cuando','donde','mientras','quien','con','entre','sin','trabajo','trabajar','trabajas','trabaja','trabajamos','trabajais','trabajan','podria','podrias','podriamos','podrian','podriais','yo','aquel','aun','no');

        return preg_replace('/\b('.implode('|',$commonWords).')\b/','',$input);
    }

    public function procesarMagicamente($file)
    {
        #aplicando la función de stip-tags
        $aux = preg_replace('/\s+/', ' ', $file);
        $aux = str_replace("á", "a", $aux);
        $aux = str_replace("é", "e", $aux);
        $aux = str_replace("í", "i", $aux);
        $aux = str_replace("ó", "o", $aux);
        $aux = str_replace("ú", "u", $aux);
        $aux = preg_replace('#<script(.*?)>(.*?)</script>#is', '', $aux);
        $aux = preg_replace('#<style(.*?)>(.*?)</style>#is', '', $aux);
        $aux = preg_replace('#&nbsp;#', '', $aux);
        $aux = preg_replace('#&quote;#', '', $aux);
        $aux = strip_tags($aux);
        $aux = preg_replace('/\s+/', ' ', $aux);
        $aux = strtolower($aux);
        $aux = $this->cleanString($aux);
        $aux = strtolower($aux);
        $aux = str_replace("'", "", $aux);
        $aux = str_replace("|", "", $aux);
        $aux = str_replace("(", "", $aux);
        $aux = str_replace(")", "", $aux);
        $aux = str_replace("@", "", $aux);
        $aux = str_replace("&", "", $aux);
        $aux = str_replace("%", "", $aux);
        $aux = str_replace("#", "", $aux);
        $aux = str_replace("+", "", $aux);
        $aux = str_replace("?", "", $aux);
        $aux = str_replace("\"", "", $aux);
        $aux = str_replace("/", "", $aux);
        $aux = str_replace(":", "", $aux);
        /* Removiendo números */
        $aux = preg_replace('/[0-9]+/', '', $aux);
        //$aux = str_replace("%", " porciento", $aux); //no es necesario porque no hay números
        $aux = preg_replace("/a{2,}/", "a", $aux);
        $aux = preg_replace("/>{2,}/", "", $aux);

        /* Removiendo palabras vacias */
        $aux = $this->removeCommonWords($aux);
        /* Volviendo a quitar espacios que se generan */
        $aux = preg_replace('/\s+/', ' ', $aux);

        return $aux;
    }

    public function agregarNoticiaAction(Request $request)
    {
        set_time_limit(0); //Para evitar el tiempo maximo de ejecución alcanzado error
        $form = $request->request->all();
        $em = $this->getDoctrine()->getManager();

        $result = $em->getRepository('AppBundle:Noticia')->existeURL($form['enlace']);
        if (sizeof($result) > 0) {
            return new JsonResponse(
                array(
                    'message' => "El recurso que intenta añadir ya se encuentra almacenado.",
                    'code' => 500)
                );
        }else{

            $recurso = $em->getRepository('AppBundle:RecursoRSS')->find($form['recurso']);;

            $noticia = new Noticia();
            $categoriaRSS = new CategoriaRSS();
            $noticia->setFecha($form['fecha']);
            $noticia->setTitulo($form['titulo']);
            $noticia->setUrl($form['enlace']);
            $noticia->setCategoria($form['categoria']);

            if(!$em->getRepository('AppBundle:CategoriaRSS')->existe_categoria($form['categoria']))
            {
                $categoriaRSS->setNombre($form['categoria']);
                $em->persist($categoriaRSS);
            }

            $noticia->setRecurso($recurso);
            $url = $form['enlace'];

            $curl = curl_init();
            curl_setopt($curl, CURLOPT_URL, $url);
            curl_setopt($curl, CURLOPT_HTTPPROXYTUNNEL, 0);
            /**************** PARA EL PROXY *******************/
             curl_setopt($curl, CURLOPT_PROXYUSERPWD, 'lperez20:kyomarotakamine');
             curl_setopt($curl, CURLOPT_PROXY, 'proxy.uc.edu.ve:5010');
            /**************** PARA EL PROXY *******************/
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($curl, CURLOPT_HEADER, false);
            $file = curl_exec($curl);
            curl_close($curl);

            $aux = $this->procesarMagicamente($file);

            $noticia->setPalabras(0);
            $em->persist($noticia);
            $em->flush();

            /*if ($form['tipo'] == 'PDF') {
                $fp = fopen($this->container->getParameter('kernel.root_dir').'/../web/uploads/archivos/noticia '.$noticia->getId().'.pdf', 'w');
                fwrite($fp, $aux);
                fclose($fp);

                return new JsonResponse(
                array(
                	'code' => 200,
                    'message' => "Se ha almacenado el recurso PDF '".$form['titulo']."', de manera correcta.")
                );
            }*/

            if ($form['tipo'] == 'RSS') {
                $fp = fopen($this->container->getParameter('kernel.root_dir').'/../web/uploads/archivos/noticia '.$noticia->getId().'.txt', 'w');
                fwrite($fp, $aux);
                fclose($fp);

                $ubicacion = $this->container->getParameter('kernel.root_dir').'/../web/uploads/archivos/noticia '.$noticia->getId().'.txt';
                $contenido = file_get_contents($ubicacion);

                $palabras = explode(' ', $contenido);
                $terminos = array();
                $lematizador = new \stemm_es();

                foreach ($palabras as $palabra) {
                    //strlen para almacenar palabras de mas de 2 letras
                    if ($palabra != '' && strlen($palabra) > 2) {
                        $palabra = $lematizador->stemm($palabra); // Se lematizan las palabras
                        array_push($terminos, $palabra); //Inserto la palabra en el array de terminos del documento actual

                        /*Para obtener la frecuencia que aparece el termino en el documento que está siendo procesado*/

                        //Puedo trasladar esta parte hacia la base de datos
                        $em->getRepository('AppBundle:TDF')->agregar_termino_tdf($palabra, $noticia->getId());
                        $termino_tdf = $em->getRepository('AppBundle:TDF')->obtener_termino($palabra, $noticia->getId());
                        if ($termino_tdf == NULL) {
                            $term = new TDF();
                            $term->setTermino($palabra);
                            $term->setDocumento($noticia);
                            $term->setFrecuencia('1');
                            $em->persist($term);
                            $em->flush();
                        }else{
                            $term = $termino_tdf[0];
                            $frecuencia = $term->getFrecuencia();
                            $frecuencia++;
                            $term->setFrecuencia($frecuencia);
                            $em->persist($term);
                            $em->flush();
                        }
                        
                        if (!in_array($palabra, $terminos)) {
                            /* Para alamacenar el número de documentos en los que aparece el termino i */
                            $termino_tcf = $em->getRepository('AppBundle:TCF')->obtener_termino($palabra);
                            if ($termino_tcf == NULL) {
                                $tCF = new TCF();
                                $tCF->setTermino($palabra);
                                $tCF->setFrecuencia('1');
                                $em->persist($tCF);
                                $em->flush();
                            }else{
                                $term = $termino_tcf[0];
                                $frecuencia = $term->getFrecuencia();
                                $frecuencia++;
                                $term->setFrecuencia($frecuencia);
                                $em->persist($term);
                                $em->flush();
                            }

                            
                        }
                    }
                }

                if (sizeof($terminos)>0) {
                    $noticia->setPalabras(sizeof($terminos)); //palabras sin repetir
                    $em->persist($noticia);
                    $em->flush();

                    /* ESTA ES LA PARTE DEL ALGORITMO QUE REPRESENTA EL MAYOR CONSUMO DE MEMORIA DE PROCESAMIENTO */
                    $N = $em->getRepository('AppBundle:Noticia')->cantidad_de_documentos();
                    $todos = $em->getRepository('AppBundle:TDF')->findAll();
                    foreach ($todos as $termino) {
                        $fij = $termino->getFrecuencia(); //cantidad de veces que aparece el termino en el documento
                        $dfi = $em->getRepository('AppBundle:TCF')->frecuencia_de_documentos($termino->getTermino());
                        //foreach ($dfi as $frecuencia){ $var = $frecuencia->getFrecuencia(); }
                        //*******************************************************
                        // WHERE THE MAGIC HAPPENS
                        $peso = $fij*log($N/$dfi);
                        //*******************************************************
                        $v = $em->getRepository('AppBundle:Vector')->obtener_termino($termino->getId());
                        if ($v == NULL) {
                            $vector = new Vector();
                            $vector->setDocumento($noticia);
                            $vector->setTerminos($termino);
                            $vector->setPeso($peso);
                        }else{
                            $vector = $v[0];
                            $vector->setPeso($peso);
                        }
                        $em->persist($vector);
                        $em->flush();
                    }

                    return new JsonResponse(
                    array(
                        'code' => 200,
                        'message' => "Se ha almacenado el recurso RSS '".$form['titulo']."', de manera correcta.")
                    );
                }else{
                    return new JsonResponse(
                    array(
                        'code' => 500,
                        'message' => "No se logró extraer nada del recurso '".$form['titulo']."', por favor intente almacenarlo otra vez.")
                    );
                }
            }
        }
    }
}
