<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use AppBundle\Entity\Vector;
use AppBundle\Form\VectorType;

/**
 * Vector controller.
 *
 */
class VectorController extends Controller
{
    /**
     * Lists all Vector entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $vectors = $em->getRepository('AppBundle:Vector')->findAll();

        return $this->render('vector/index.html.twig', array(
            'vectors' => $vectors,
        ));
    }

    /**
     * Creates a new Vector entity.
     *
     */
    public function newAction(Request $request)
    {
        $vector = new Vector();
        $form = $this->createForm('AppBundle\Form\VectorType', $vector);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($vector);
            $em->flush();

            return $this->redirectToRoute('vector_show', array('id' => $vector->getId()));
        }

        return $this->render('vector/new.html.twig', array(
            'vector' => $vector,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Vector entity.
     *
     */
    public function showAction(Vector $vector)
    {
        $deleteForm = $this->createDeleteForm($vector);

        return $this->render('vector/show.html.twig', array(
            'vector' => $vector,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Vector entity.
     *
     */
    public function editAction(Request $request, Vector $vector)
    {
        $deleteForm = $this->createDeleteForm($vector);
        $editForm = $this->createForm('AppBundle\Form\VectorType', $vector);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($vector);
            $em->flush();

            return $this->redirectToRoute('vector_edit', array('id' => $vector->getId()));
        }

        return $this->render('vector/edit.html.twig', array(
            'vector' => $vector,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a Vector entity.
     *
     */
    public function deleteAction(Request $request, Vector $vector)
    {
        $form = $this->createDeleteForm($vector);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($vector);
            $em->flush();
        }

        return $this->redirectToRoute('vector_index');
    }

    /**
     * Creates a form to delete a Vector entity.
     *
     * @param Vector $vector The Vector entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Vector $vector)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('vector_delete', array('id' => $vector->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
