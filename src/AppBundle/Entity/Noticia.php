<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Noticia
 *
 * @ORM\Table(name="noticia")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\NoticiaRepository")
 */
class Noticia
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="fecha", type="string", length=255)
     */
    private $fecha;

    /**
     * @var string
     *
     * @ORM\Column(name="titulo", type="string", length=255)
     */
    private $titulo;

    /**
     * @var string
     *
     * @ORM\Column(name="url", type="text")
     */
    private $url;

    /**
     * @var integer
     * 
     * @ORM\Column(name="palabras", type="integer")
     */
    private $palabras;

    /**
     * @var string
     *
     * @ORM\Column(name="categoria", type="text")
     */
    private $categoria;

    /**
     * @ORM\ManyToOne(targetEntity="RecursoRSS", inversedBy="noticias")
     * @ORM\JoinColumn(name="recurso_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $recurso;

    /**
     * @ORM\OneToMany(targetEntity="TDF", mappedBy="documento")
     */
    private $terminos;

    /**
     * @ORM\OneToMany(targetEntity="Vector", mappedBy="documento")
     */
    private $vector;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set titulo
     *
     * @param string $titulo
     * @return Noticia
     */
    public function setTitulo($titulo)
    {
        $this->titulo = $titulo;

        return $this;
    }

    /**
     * Get titulo
     *
     * @return string 
     */
    public function getTitulo()
    {
        return $this->titulo;
    }

    /**
     * Set url
     *
     * @param string $url
     * @return Noticia
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Get url
     *
     * @return string 
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Set categoria
     *
     * @param string $categoria
     * @return Noticia
     */
    public function setCategoria($categoria)
    {
        $this->categoria = $categoria;

        return $this;
    }

    /**
     * Get categoria
     *
     * @return string 
     */
    public function getCategoria()
    {
        return $this->categoria;
    }

    /**
     * Set fecha
     *
     * @param string $fecha
     * @return Noticia
     */
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;

        return $this;
    }

    /**
     * Get fecha
     *
     * @return string 
     */
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * Set recurso
     *
     * @param \AppBundle\Entity\RecursoRSS $recurso
     * @return Noticia
     */
    public function setRecurso(\AppBundle\Entity\RecursoRSS $recurso = null)
    {
        $this->recurso = $recurso;

        return $this;
    }

    /**
     * Get recurso
     *
     * @return \AppBundle\Entity\RecursoRSS 
     */
    public function getRecurso()
    {
        return $this->recurso;
    }

    /**
     * Set palabras
     *
     * @param integer $palabras
     * @return Noticia
     */
    public function setPalabras($palabras)
    {
        $this->palabras = $palabras;

        return $this;
    }

    /**
     * Get palabras
     *
     * @return integer 
     */
    public function getPalabras()
    {
        return $this->palabras;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->terminos = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add terminos
     *
     * @param \AppBundle\Entity\TDF $terminos
     * @return Noticia
     */
    public function addTermino(\AppBundle\Entity\TDF $terminos)
    {
        $this->terminos[] = $terminos;

        return $this;
    }

    /**
     * Remove terminos
     *
     * @param \AppBundle\Entity\TDF $terminos
     */
    public function removeTermino(\AppBundle\Entity\TDF $terminos)
    {
        $this->terminos->removeElement($terminos);
    }

    /**
     * Get terminos
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getTerminos()
    {
        return $this->terminos;
    }

    /**
     * Add vector
     *
     * @param \AppBundle\Entity\Vector $vector
     * @return Noticia
     */
    public function addVector(\AppBundle\Entity\Vector $vector)
    {
        $this->vector[] = $vector;

        return $this;
    }

    /**
     * Remove vector
     *
     * @param \AppBundle\Entity\Vector $vector
     */
    public function removeVector(\AppBundle\Entity\Vector $vector)
    {
        $this->vector->removeElement($vector);
    }

    /**
     * Get vector
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getVector()
    {
        return $this->vector;
    }
}
