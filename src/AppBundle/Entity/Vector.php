<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Vector
 *
 * @ORM\Table(name="vector")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\VectorRepository")
 */
class Vector
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Noticia", inversedBy="vector")
     * @ORM\JoinColumn(name="noticia_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $documento;

    /**
     * @ORM\ManyToOne(targetEntity="TDF", inversedBy="vector")
     * @ORM\JoinColumn(name="termino_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $terminos;
    
    /**
     * @var float
     *
     * @ORM\Column(name="peso", type="float")
     */
    private $peso;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set peso
     *
     * @param float $peso
     * @return Vector
     */
    public function setPeso($peso)
    {
        $this->peso = $peso;

        return $this;
    }

    /**
     * Get peso
     *
     * @return float 
     */
    public function getPeso()
    {
        return $this->peso;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->terminos = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add terminos
     *
     * @param \AppBundle\Entity\TDF $terminos
     * @return Vector
     */
    public function addTermino(\AppBundle\Entity\TDF $terminos)
    {
        $this->terminos[] = $terminos;

        return $this;
    }

    /**
     * Remove terminos
     *
     * @param \AppBundle\Entity\TDF $terminos
     */
    public function removeTermino(\AppBundle\Entity\TDF $terminos)
    {
        $this->terminos->removeElement($terminos);
    }

    /**
     * Get terminos
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getTerminos()
    {
        return $this->terminos;
    }

    /**
     * Set documento
     *
     * @param \AppBundle\Entity\Noticia $documento
     * @return Vector
     */
    public function setDocumento(\AppBundle\Entity\Noticia $documento = null)
    {
        $this->documento = $documento;

        return $this;
    }

    /**
     * Get documento
     *
     * @return \AppBundle\Entity\Noticia 
     */
    public function getDocumento()
    {
        return $this->documento;
    }

    /**
     * Set vector
     *
     * @param \AppBundle\Entity\Vector $vector
     * @return Vector
     */
    public function setVector(\AppBundle\Entity\Vector $vector = null)
    {
        $this->vector = $vector;

        return $this;
    }

    /**
     * Get vector
     *
     * @return \AppBundle\Entity\Vector 
     */
    public function getVector()
    {
        return $this->vector;
    }

    /**
     * Set terminos
     *
     * @param \AppBundle\Entity\TDF $terminos
     * @return Vector
     */
    public function setTerminos(\AppBundle\Entity\TDF $terminos = null)
    {
        $this->terminos = $terminos;

        return $this;
    }
}
