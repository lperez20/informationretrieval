<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * TDF
 *
 * @ORM\Table(name="t_d_f")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\TDFRepository")
 * @UniqueEntity(
 *     fields={"termino","documento"}
 * )
 */
class TDF
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="termino", type="string", length=255)
     */
    private $termino;

    
    /**
     * @ORM\ManyToOne(targetEntity="Noticia", inversedBy="terminos")
     * @ORM\JoinColumn(name="noticia_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $documento;

    /**
     * @var int
     *
     * @ORM\Column(name="frecuencia", type="integer")
     */
    private $frecuencia;

    /**
     * @ORM\OneToMany(targetEntity="Vector", mappedBy="terminos")
     */
    private $vector;

    public function __toString()
    {
        return $this->documento->getTitulo();
    }


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set termino
     *
     * @param string $termino
     * @return TDF
     */
    public function setTermino($termino)
    {
        $this->termino = $termino;

        return $this;
    }

    /**
     * Get termino
     *
     * @return string 
     */
    public function getTermino()
    {
        return $this->termino;
    }

    /**
     * Set documento
     *
     * @param \stdClass $documento
     * @return TDF
     */
    public function setDocumento($documento)
    {
        $this->documento = $documento;

        return $this;
    }

    /**
     * Get documento
     *
     * @return \stdClass 
     */
    public function getDocumento()
    {
        return $this->documento;
    }

    /**
     * Set frecuencia
     *
     * @param integer $frecuencia
     * @return TDF
     */
    public function setFrecuencia($frecuencia)
    {
        $this->frecuencia = $frecuencia;

        return $this;
    }

    /**
     * Get frecuencia
     *
     * @return integer 
     */
    public function getFrecuencia()
    {
        return $this->frecuencia;
    }
}
